fonts-horai-umefont (670-5) unstable; urgency=medium

  * debian/{fonts-horai-umefont.install,lintian/fonts-horai-umefont}
    - Drop unnecessary lintian-overrides
  * debian/changelog
    - Trim trailing whitespace.
  * Remove debian/source/options
    - Drop custom source compression, custom source compression level.
      (Note: Still keep custom compression level, however, since it keeps as
       7MB vs 25MB with default compression).
  * debian/control
    - Update standards version to 4.5.0, no changes needed.
    - Use dh13

 -- Hideki Yamane <henrich@debian.org>  Sun, 15 Nov 2020 19:19:19 +0900

fonts-horai-umefont (670-4) unstable; urgency=medium

  * debian/control
    - use dh12
    - set Standards-Version: 4.4.1
    - drop Replaces and Breaks to old ttf-umefont package
    - drop old fonts-horai-umefont.preinst file for defoma
    - drop Pre-Depends as well
    - set Rules-Requires-Root: no
  * debian/copyright
    - use https
    - update copyright year
  * add debian/salsa-ci.yml

 -- Hideki Yamane <henrich@debian.org>  Mon, 14 Oct 2019 22:59:40 +0900

fonts-horai-umefont (670-3) unstable; urgency=medium

  * debian/control
    - Update Vcs-* to use salsa.debian.org
    - Update Maintainer address
    - Set Standards-Version: 4.1.4
  * debian/watch
    - Deal with upstream's change
  * Use dh11

 -- Hideki Yamane <henrich@debian.org>  Fri, 01 Jun 2018 08:28:38 +0900

fonts-horai-umefont (670-2) unstable; urgency=medium

  * debian/control
    - Build-Depends: debhelper (>= 10)
    - set Standards-Version: 4.0.0
    - update Homepage: to osdn.net
  * debian/compat
    - set 10
  * debian/copyright
    - update Upstream Source URL to osdn.net
  * debian/watch
    - update to version 4

 -- Hideki Yamane <henrich@debian.org>  Sun, 16 Jul 2017 21:02:21 +0900

fonts-horai-umefont (670-1) unstable; urgency=medium

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Sun, 02 Apr 2017 11:05:16 +0900

fonts-horai-umefont (660-1) unstable; urgency=medium

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Sat, 03 Sep 2016 11:49:22 +0900

fonts-horai-umefont (641-1) unstable; urgency=medium

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Sun, 07 Aug 2016 14:29:02 +0900

fonts-horai-umefont (630-1) unstable; urgency=medium

  * New upstream release with fix regression

 -- Hideki Yamane <henrich@debian.org>  Sat, 18 Jun 2016 14:33:44 +0900

fonts-horai-umefont (620-1) unstable; urgency=medium

  * New upstream release
  * debian/control
    - add new Ume Hy Gothic to description
    - set Standards-Version: 3.9.8

 -- Hideki Yamane <henrich@debian.org>  Sun, 05 Jun 2016 09:17:24 +0900

fonts-horai-umefont (610-1) unstable; urgency=medium

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Thu, 31 Mar 2016 19:31:54 +0900

fonts-horai-umefont (600-1) unstable; urgency=medium

  * New upstream release
  * debian/control
    - use https for upstream homepage
    - set Standards-Version: 3.9.7

 -- Hideki Yamane <henrich@debian.org>  Wed, 02 Mar 2016 07:20:29 +0900

fonts-horai-umefont (590-1) unstable; urgency=medium

  * New upstream release
  * debian/control
    - use https for Vcs-*

 -- Hideki Yamane <henrich@debian.org>  Mon, 01 Feb 2016 21:26:47 +0900

fonts-horai-umefont (580-1) unstable; urgency=medium

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Thu, 31 Dec 2015 17:29:01 +0900

fonts-horai-umefont (560-2) unstable; urgency=medium

  * fix fonts-horai-umefont.preinst

 -- Hideki Yamane <henrich@debian.org>  Wed, 23 Dec 2015 12:57:06 +0900

fonts-horai-umefont (560-1) unstable; urgency=medium

  * Imported Upstream version 560

 -- Hideki Yamane <henrich@debian.org>  Sun, 01 Nov 2015 14:28:34 +0900

fonts-horai-umefont (550-1) unstable; urgency=medium

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Wed, 07 Oct 2015 22:13:23 +0900

fonts-horai-umefont (540-1) unstable; urgency=medium

  * Imported Upstream version 540

 -- Hideki Yamane <henrich@debian.org>  Tue, 01 Sep 2015 04:31:17 +0900

fonts-horai-umefont (530-1) unstable; urgency=medium

  * Imported Upstream version 530

 -- Hideki Yamane <henrich@debian.org>  Sat, 01 Aug 2015 20:20:49 +0900

fonts-horai-umefont (520-1) unstable; urgency=medium

  * Imported Upstream version 520
  * debian/{control,watch,copyright}
    - sourceforge.jp -> osdn.jp
  * debian/rules
    - remove exec bit from upstream files in build time

 -- Hideki Yamane <henrich@debian.org>  Wed, 01 Jul 2015 14:03:39 +0900

fonts-horai-umefont (510-1) unstable; urgency=medium

  * Imported Upstream version 510
  * debian/fonts-horai-umefont.preinst
    - fix "command-with-path-in-maintainer-script" lintian warning

 -- Hideki Yamane <henrich@debian.org>  Tue, 26 May 2015 20:05:29 +0900

fonts-horai-umefont (500-1) unstable; urgency=medium

  * Imported Upstream version 500

 -- Hideki Yamane <henrich@debian.org>  Wed, 29 Apr 2015 23:13:18 +0900

fonts-horai-umefont (490-1) unstable; urgency=medium

  * New Upstream version 490

 -- Hideki Yamane <henrich@debian.org>  Thu, 02 Apr 2015 21:31:01 +0900

fonts-horai-umefont (480-1) unstable; urgency=medium

  * New Upstream version 480

 -- Hideki Yamane <henrich@debian.org>  Mon, 02 Mar 2015 06:11:18 +0900

fonts-horai-umefont (470-1) unstable; urgency=medium

  * New Upstream version 470
  * add lintian-overrides due to license.html is just a license text but
    lintian warnings about it, so ignore it.

 -- Hideki Yamane <henrich@debian.org>  Sun, 01 Feb 2015 13:06:56 +0900

fonts-horai-umefont (468-1) unstable; urgency=medium

  * New Upstream version 468

 -- Hideki Yamane <henrich@debian.org>  Mon, 29 Dec 2014 23:52:10 +0900

fonts-horai-umefont (467-1) unstable; urgency=medium

  * New upstream release
  * debian/control
    - set Standards-Version: 3.9.6
    - update Vcs-Browser

 -- Hideki Yamane <henrich@debian.org>  Thu, 27 Nov 2014 08:02:51 +0900

fonts-horai-umefont (465-1) unstable; urgency=medium

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Mon, 15 Sep 2014 11:14:25 +0900

fonts-horai-umefont (464-1) unstable; urgency=medium

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Sat, 09 Aug 2014 21:31:51 +0900

fonts-horai-umefont (463-1) unstable; urgency=medium

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Mon, 02 Jun 2014 20:58:14 +0900

fonts-horai-umefont (462-1) unstable; urgency=medium

  * New upstream release
  * debian/source/options
    - add compression option. However, compress-strategy does not work so
      commented out.

 -- Hideki Yamane <henrich@debian.org>  Sat, 19 Apr 2014 09:30:56 +0900

fonts-horai-umefont (461-1) unstable; urgency=medium

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Thu, 10 Apr 2014 22:14:37 +0900

fonts-horai-umefont (460-1) unstable; urgency=medium

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Tue, 04 Feb 2014 23:30:55 +0900

fonts-horai-umefont (459-1) unstable; urgency=medium

  * New upstream release
  * set "Standards-Version: 3.9.5" with no changes

 -- Hideki Yamane <henrich@debian.org>  Tue, 07 Jan 2014 07:54:53 +0900

fonts-horai-umefont (458-1) unstable; urgency=medium

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Fri, 06 Dec 2013 00:51:30 +0900

fonts-horai-umefont (457-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Thu, 14 Nov 2013 21:29:53 +0900

fonts-horai-umefont (456-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Tue, 08 Oct 2013 19:30:41 +0900

fonts-horai-umefont (455-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Sat, 07 Sep 2013 20:42:46 +0900

fonts-horai-umefont (454-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Mon, 05 Aug 2013 08:40:41 +0900

fonts-horai-umefont (453-1) unstable; urgency=low

  * New upstream release
  * debian/control
    - use canonical URL for Vcs-*

 -- Hideki Yamane <henrich@debian.org>  Mon, 01 Jul 2013 09:09:02 +0900

fonts-horai-umefont (452-1) unstable; urgency=low

  * New upstream release
  * debian/control: drop transitional ttf- package

 -- Hideki Yamane <henrich@debian.org>  Thu, 06 Jun 2013 23:27:29 +0900

fonts-horai-umefont (451-1) unstable; urgency=low

  * New upstream release
  * debian/control
    - move from svn to git for Vcs-* field
    - set "Multi-Arch: foreign" (Closes: #708938)
  * upload to unstable

 -- Hideki Yamane <henrich@debian.org>  Sun, 02 Jun 2013 15:10:18 +0900

fonts-horai-umefont (450-1) experimental; urgency=low

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Tue, 02 Apr 2013 05:54:09 +0900

fonts-horai-umefont (449-1) experimental; urgency=low

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Sat, 23 Mar 2013 19:55:46 +0900

fonts-horai-umefont (448-1) experimental; urgency=low

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Sat, 23 Mar 2013 19:54:44 +0900

fonts-horai-umefont (447-1) experimental; urgency=low

  * New upstream release
  * debian/control
    - set "Standards-Version: 3.9.4"

 -- Hideki Yamane <henrich@debian.org>  Wed, 09 Jan 2013 00:46:32 +0900

fonts-horai-umefont (446-1) experimental; urgency=low

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Tue, 11 Dec 2012 11:55:58 +0900

fonts-horai-umefont (445-1) experimental; urgency=low

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Sun, 18 Nov 2012 22:04:21 +0900

fonts-horai-umefont (444-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Sat, 06 Oct 2012 20:56:14 +0900

fonts-horai-umefont (443-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Thu, 06 Sep 2012 09:24:24 +0900

fonts-horai-umefont (442-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Mon, 06 Aug 2012 12:58:15 +0900

fonts-horai-umefont (441-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Wed, 04 Jul 2012 08:49:58 +0900

fonts-horai-umefont (440-3) unstable; urgency=low

  * debian/control
    - change from Conflicts: to Breaks: to be Policy polite

 -- Hideki Yamane <henrich@debian.org>  Thu, 21 Jun 2012 09:17:48 +0900

fonts-horai-umefont (440-2) unstable; urgency=low

  * debian/control
    - Bump debhelper version to deal with xz option properly.

 -- Hideki Yamane <henrich@debian.org>  Tue, 12 Jun 2012 17:03:34 +0900

fonts-horai-umefont (440-1) unstable; urgency=low

  * New upstream release
  * debian/control
    - add "Pre-Depends: dpkg (>= 1.15.6~)" for xz
  * debian/copyright
    - update to "Machine-readable debian/copyright file 1.0"
  * debian/lintian/fonts-horai-umefont
    - remove to fix "unused-override"
    - add "fonts-horai-umefont: no-upstream-changelog"

 -- Hideki Yamane <henrich@debian.org>  Tue, 05 Jun 2012 05:45:02 +0900

fonts-horai-umefont (439-3) unstable; urgency=low

  * debian/rules
    - add option to xz

 -- Hideki Yamane <henrich@debian.org>  Sat, 02 Jun 2012 03:54:57 +0900

fonts-horai-umefont (439-2) unstable; urgency=low

  * debian/rules
    - add override_dh_builddeb to reduce package size

 -- Hideki Yamane <henrich@debian.org>  Thu, 24 May 2012 10:24:58 +0900

fonts-horai-umefont (439-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Sun, 13 May 2012 23:34:00 +0900

fonts-horai-umefont (437-1) unstable; urgency=low

  * New upstream release
  * debian/control
    - set "Standards-Version: 3.9.3" with no change

 -- Hideki Yamane <henrich@debian.org>  Thu, 08 Mar 2012 07:53:46 +0900

fonts-horai-umefont (436-1) unstable; urgency=low

  * New upstream release
  * debian/control
    - set ttf-umefont as "Priority: extra"

 -- Hideki Yamane <henrich@debian.org>  Mon, 06 Feb 2012 23:30:25 +0900

fonts-horai-umefont (435-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Tue, 24 Jan 2012 03:52:49 +0900

fonts-horai-umefont (434-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Mon, 05 Dec 2011 22:09:29 +0900

fonts-horai-umefont (433-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Mon, 07 Nov 2011 20:40:34 +0900

fonts-horai-umefont (432-1) unstable; urgency=low

  * New upstream release
  * debian/copyright
    - fix copyright year for debian/* files

 -- Hideki Yamane <henrich@debian.org>  Mon, 03 Oct 2011 21:37:52 +0900

fonts-horai-umefont (431-2) unstable; urgency=low

  * debian/copyright
    - fix "syntax-error-in-dep5-copyright"
  * debian/fonts-horai-umefont.install
    - revert install path

 -- Hideki Yamane <henrich@debian.org>  Mon, 19 Sep 2011 16:54:01 +0900

fonts-horai-umefont (431-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Thu, 08 Sep 2011 06:02:40 +0900

fonts-horai-umefont (430-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Sat, 20 Aug 2011 18:56:26 +0900

fonts-horai-umefont (429-2) unstable; urgency=low

  * debian/control
    - introduce ttf-umefont as transitional dummy package
    - fix Vcs-* field

 -- Hideki Yamane <henrich@debian.org>  Fri, 29 Jul 2011 13:25:55 +0200

fonts-horai-umefont (429-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Tue, 12 Jul 2011 06:06:27 +0900

fonts-horai-umefont (428-1) unstable; urgency=low

  * New upstream release
  * debian/lintian/overide
    - add override to ignore warning.

 -- Hideki Yamane <henrich@debian.org>  Thu, 09 Jun 2011 06:28:52 +0900

fonts-horai-umefont (426-1) unstable; urgency=low

  * New upstream release
  * update package name to new font package naming scheme
  * update debian/copyright
  * debian/control
    - update Standards-Version: 3.9.2 without changes.

 -- Hideki Yamane <henrich@debian.org>  Wed, 04 May 2011 07:56:57 +0900

ttf-umefont (425-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Tue, 08 Mar 2011 07:09:38 +0900

ttf-umefont (424-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Tue, 22 Feb 2011 22:38:08 +0900

ttf-umefont (423-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Tue, 04 Jan 2011 20:25:12 +0900

ttf-umefont (422-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Tue, 07 Dec 2010 23:02:41 +0900

ttf-umefont (421-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Wed, 03 Nov 2010 23:24:16 +0900

ttf-umefont (420-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Mon, 04 Oct 2010 22:23:28 +0900

ttf-umefont (419-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Wed, 08 Sep 2010 23:14:26 +0900

ttf-umefont (418-1) unstable; urgency=low

  * New upstream release
  * debian/control
    - Bump up "Standards-Version: 3.9.1" with no change
  * debian/watch
    - adjust to check upstream version correctly, it's not only numbers.

 -- Hideki Yamane <henrich@debian.org>  Mon, 02 Aug 2010 12:31:57 -0400

ttf-umefont (417-1) unstable; urgency=low

  * New upstream release
  * debian/control
    - Bump up "Standards-Version: 3.9.0" with no change

 -- Hideki Yamane <henrich@debian.org>  Wed, 07 Jul 2010 01:19:00 +0900

ttf-umefont (416-2) unstable; urgency=low

  * remove excutable flag from files.

 -- Hideki Yamane <henrich@debian.org>  Sun, 20 Jun 2010 13:34:43 +0900

ttf-umefont (416-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Tue, 15 Jun 2010 00:58:47 +0900

ttf-umefont (415-1) unstable; urgency=low

  * New upstream release
  * debian/control
    - update my address
    - set "Build-Depends: debhelper (>= 7.0.50~)
    - remove "DM-Upload-Allowed: yes"

 -- Hideki Yamane <henrich@debian.org>  Wed, 05 May 2010 11:17:18 +0900

ttf-umefont (414-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Tue, 06 Apr 2010 17:14:50 +0900

ttf-umefont (413-2) unstable; urgency=low

  * fix wrong changelog revision entry.

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Thu, 04 Mar 2010 07:41:39 +0900

ttf-umefont (413-1) unstable; urgency=low

  * New upstream release
  * debian/watch:
    - deal with 7zip archive.

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Tue, 02 Mar 2010 22:06:08 +0900

ttf-umefont (412-1) unstable; urgency=low

  * New upstream release
  * debian/control:
    - set "Standards-Version: 3.8.4" with no change

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Tue, 02 Feb 2010 06:29:43 +0900

ttf-umefont (411-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Tue, 05 Jan 2010 21:21:07 +0900

ttf-umefont (410-2) unstable; urgency=low

  * fix preinst script (set -e)
  * debian/control: drop "Suggests:" field.

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Fri, 18 Dec 2009 09:56:22 +0900

ttf-umefont (410-1) unstable; urgency=low

  * New upstream release
  * Swithc to debhelper7 style
  * Switch to 3.0 (quilt) source format
  * Drop defoma use

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Fri, 11 Dec 2009 05:45:33 +0900

ttf-umefont (409-2) unstable; urgency=low

  * rebuild it to avoid defoma's regression.

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Sun, 22 Nov 2009 06:32:37 +0900

ttf-umefont (409-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Tue, 10 Nov 2009 10:09:10 +0900

ttf-umefont (408-2) unstable; urgency=low

  * debian/copyright:
   - fix wrong "Maintainer:" field.

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Tue, 06 Oct 2009 08:17:42 +0900

ttf-umefont (408-1) unstable; urgency=low

  * New upstream release
  * debian/copyright:
    - convert to "Machine-readable debian/copyright"

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Tue, 06 Oct 2009 07:49:23 +0900

ttf-umefont (407-1) unstable; urgency=low

  * New upstream release
  * debian/control:
    - set "Starndards-Version: 3.8.3"

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Thu, 10 Sep 2009 22:32:20 +0900

ttf-umefont (406-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Mon, 03 Aug 2009 08:31:45 +0900

ttf-umefont (405-1) unstable; urgency=low

  * New upstream release
  * debian/watch: update it to deal with sourceforge.jp's change

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Wed, 08 Jul 2009 20:46:12 +0900

ttf-umefont (404-3) unstable; urgency=low

  * rebuild to build postinst and prerm scripts correctly.

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Thu, 25 Jun 2009 20:23:58 +0900

ttf-umefont (404-2) unstable; urgency=low

  * debian/{postinst,prerm}:
    - remove fc-cache.
  * debian/control:
    - set "Starndards-Version: 3.8.2"
    - "Suggests: fontconfig (>= 2.6.0-4)"

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Sun, 21 Jun 2009 00:56:45 +0900

ttf-umefont (404-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Tue, 02 Jun 2009 22:31:48 +0900

ttf-umefont (403-1) unstable; urgency=low

  * New upstream release
    - New license, it based on hanazono mincho and glyphwiki.
      So it's still DFSG-free one.
  * debian/copyright: update it as well
  * debian/docs: remove it.
  * debian/ttf-umefont.defoma-hints: add new 6 fonts.

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Sun, 10 May 2009 03:14:58 +0900

ttf-umefont (402-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Tue, 07 Apr 2009 05:22:38 +0900

ttf-umefont (401-2) unstable; urgency=low

  * debian/control:
    - new section "fonts"
    - Standards-Version: 3.8.1

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Wed, 25 Mar 2009 07:09:24 +0900

ttf-umefont (401-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Tue, 03 Mar 2009 19:35:21 +0900

ttf-umefont (400-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Mon, 02 Feb 2009 23:41:10 +0900

ttf-umefont (399-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Mon, 12 Jan 2009 08:22:58 +0900

ttf-umefont (398-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Tue, 06 Jan 2009 03:34:10 +0900

ttf-umefont (397-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Mon, 01 Dec 2008 23:29:06 +0900

ttf-umefont (396-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Fri, 28 Nov 2008 21:16:47 +0900

ttf-umefont (395-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Tue, 04 Nov 2008 08:07:44 +0900

ttf-umefont (394-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Mon, 06 Oct 2008 23:59:20 +0900

ttf-umefont (393-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Tue, 02 Sep 2008 00:08:45 +0900

ttf-umefont (392-1) unstable; urgency=low

  * New upstream release
  * debian/postinst,prerm
    - tweak them to fix lintian warning.

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Fri, 22 Aug 2008 11:31:55 +0900

ttf-umefont (391-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Thu, 17 Jul 2008 02:54:09 +0900

ttf-umefont (389-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Sat, 31 May 2008 22:16:03 +0900

ttf-umefont (387-2) unstable; urgency=low

  * debian/watch
    - deal with sourceforge.jp's change.

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Sat, 31 May 2008 22:12:36 +0900

ttf-umefont (387-1) unstable; urgency=low

  * New upstream release
  * debian/control
    - add "DM-Upload-Allowed: yes"

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Sat, 19 Apr 2008 09:31:34 +0900

ttf-umefont (386-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Fri, 28 Mar 2008 07:27:23 +0900

ttf-umefont (385-1) unstable; urgency=low

  * New upstream release
  * debian/control
    - use Build-Depends-Indep: field for defoma, not Build-Depends:.
      Thanks to lintian.
    - Suggests: xfs (>= 1:1.0.1-5)
  * debian/watch
    - improve to ease updating.

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Fri, 14 Mar 2008 21:33:33 +0900

ttf-umefont (383-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Tue, 05 Feb 2008 06:46:06 +0900

ttf-umefont (382-1) unstable; urgency=low

  * Initial release (Closes: #461757).

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Wed, 02 Jan 2008 00:27:30 +0900
